package com.example.finaluriProeqti

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_collection.*
import kotlinx.android.synthetic.main.activity_photo.*
import kotlinx.android.synthetic.main.activity_photo.photoView

class collectionActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_collection)
        supportActionBar?.hide()


        val photo = intent.extras?.getString("value")

        if(photo == "wallpaper"){
            collection1.setImageResource(R.drawable.wall1)
            collection2.setImageResource(R.drawable.wall2)
            collection3.setImageResource(R.drawable.wall3)
            collection4.setImageResource(R.drawable.wall4)
            collection5.setImageResource(R.drawable.wall5)
            collection6.setImageResource(R.drawable.wall6)
            collection7.setImageResource(R.drawable.wall7)
            collection8.setImageResource(R.drawable.wall8)
            collection9.setImageResource(R.drawable.wall9)
            collection10.setImageResource(R.drawable.wall10)

            collection1.setOnClickListener {
                goToCollection("wallpaper", "wall1")
            }
            collection2.setOnClickListener {
                goToCollection("wallpaper", "wall2")
            }
            collection6.setOnClickListener {
                goToCollection("wallpaper", "wall6")
            }
            collection7.setOnClickListener {
                goToCollection("wallpaper", "wall7")
            }



        }
        else if(photo == "fashion"){
            collection1.setImageResource(R.drawable.fashion1)
            collection2.setImageResource(R.drawable.fashion2)
            collection3.setImageResource(R.drawable.fashion3)
            collection4.setImageResource(R.drawable.fashion4)
            collection5.setImageResource(R.drawable.fashion5)
            collection6.setImageResource(R.drawable.fashion6)
            collection7.setImageResource(R.drawable.fashion7)
            collection8.setImageResource(R.drawable.fashion8)
            collection9.setImageResource(R.drawable.fashion9)
            collection10.setImageResource(R.drawable.fashion10)

            collection1.setOnClickListener {
                goToCollection("fashion", "fashion1")
            }
            collection2.setOnClickListener {
                goToCollection("fashion", "fashion2")
            }
            collection6.setOnClickListener {
                goToCollection("fashion", "fashion6")
            }
            collection7.setOnClickListener {
                goToCollection("fashion", "fashion7")
            }


        }else if(photo == "christmas"){
            collection1.setImageResource(R.drawable.christmas1)
            collection2.setImageResource(R.drawable.christmas2)
            collection3.setImageResource(R.drawable.christmas3)
            collection4.setImageResource(R.drawable.christmas4)
            collection5.setImageResource(R.drawable.christmas5)
            collection6.setImageResource(R.drawable.christmas6)
            collection7.setImageResource(R.drawable.christmas7)
            collection8.setImageResource(R.drawable.christmas8)
            collection9.setImageResource(R.drawable.christmas9)
            collection10.setImageResource(R.drawable.christmas10)

            collection1.setOnClickListener {
                goToCollection("christmas", "christmas1")
            }
            collection2.setOnClickListener {
                goToCollection("christmas", "christmas2")
            }
            collection6.setOnClickListener {
                goToCollection("christmas", "christmas6")
            }
            collection7.setOnClickListener {
                goToCollection("christmas", "christmas7")
            }


        }else if(photo == "animation"){
            collection1.setImageResource(R.drawable.animation1)
            collection2.setImageResource(R.drawable.animation2)
            collection3.setImageResource(R.drawable.animation3)
            collection4.setImageResource(R.drawable.animation4)
            collection5.setImageResource(R.drawable.animation5)
            collection6.setImageResource(R.drawable.animation6)
            collection7.setImageResource(R.drawable.animation7)
            collection8.setImageResource(R.drawable.animation8)
            collection9.setImageResource(R.drawable.animation9)
            collection10.setImageResource(R.drawable.animation10)

            collection1.setOnClickListener {
                goToCollection("animation", "animation1")
            }
            collection2.setOnClickListener {
                goToCollection("animation", "animation2")
            }
            collection6.setOnClickListener {
                goToCollection("animation", "animation6")
            }
            collection7.setOnClickListener {
                goToCollection("animation", "animation7")
            }


        }


    }

    fun goToCollection(collectionName: String, photoName: String) {
        val myActivity = (Intent(this, PhotoActivity::class.java))
        myActivity.putExtra("collection", collectionName)
        myActivity.putExtra("photoName", photoName)
        startActivity(myActivity)
    }


}