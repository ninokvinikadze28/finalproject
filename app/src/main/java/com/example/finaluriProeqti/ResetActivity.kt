package com.example.finaluriProeqti

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_reset.*

class ResetActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reset)
        supportActionBar?.hide()

        auth = FirebaseAuth.getInstance()


        sendButton.setOnClickListener {
            val email = resetEmailEditText.text.toString()
            if (email.isEmpty()) {
                Toast.makeText(this, "შეიყვანეთ ელფოსტა", Toast.LENGTH_LONG).show()
            } else {
                auth.sendPasswordResetEmail(email).addOnCompleteListener { task ->
                    if(task.isSuccessful) {
                        startActivity(Intent(this, LoginActivity::class.java))
                        finish()
                    } else {
                        Toast.makeText(this, "შეიყვანეთ სწორი ელფოსტა", Toast.LENGTH_LONG).show()
                    }
                }
            }
        }

    }
}