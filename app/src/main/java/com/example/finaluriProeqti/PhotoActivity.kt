package com.example.finaluriProeqti

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.activity_photo.*

class PhotoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_photo)
        supportActionBar?.hide()

        val photo = intent.extras?.getString("value")
        val collectionName = intent.extras?.getString("collection")
        val photoName = intent.extras?.getString("photoName")
        if (photo == "home1") {
            photoView.setBackgroundResource(R.drawable.home1)
            favoriteBtnClick()
            backButton()
        }
        else if (photo == "home2") {
            photoView.setBackgroundResource(R.drawable.home2)
            favoriteBtnClick()
            backButton()
        }
        else if (photo == "home3") {
            photoView.setBackgroundResource(R.drawable.home3)
            favoriteBtnClick()
            backButton()
        }
        else if (photo == "home4") {
            photoView.setBackgroundResource(R.drawable.home4)
            favoriteBtnClick()
            backButton()
        }
        else if (photo == "home5") {
            photoView.setBackgroundResource(R.drawable.home5)
            favoriteBtnClick()
            backButton()
        }
        else if (photo == "home6") {
            photoView.setBackgroundResource(R.drawable.home6)
            favoriteBtnClick()
            backButton()
        }
        else if (photo == "home7") {
            photoView.setBackgroundResource(R.drawable.home7)
            favoriteBtnClick()
            backButton()
        }
        else if (photo == "home8") {
            photoView.setBackgroundResource(R.drawable.home8)
            favoriteBtnClick()
            backButton()
        }
        else  if (photo == "home9") {
            photoView.setBackgroundResource(R.drawable.home9)
            favoriteBtnClick()
            backButton()
        }
        else   if (photo == "home10") {
            photoView.setBackgroundResource(R.drawable.home10)
            favoriteBtnClick()
            backButton()
        }


        else if (collectionName == "wallpaper" && photoName == "wall1") {
            photoView.setBackgroundResource(R.drawable.wall1)
            favoriteBtnClick()
            backButton()
        }
        else if (collectionName == "wallpaper" && photoName == "wall2") {
            photoView.setBackgroundResource(R.drawable.wall2)
            favoriteBtnClick()
            backButton()
        }
        else if (collectionName == "wallpaper" && photoName == "wall6") {
            photoView.setBackgroundResource(R.drawable.wall6)
            favoriteBtnClick()
            backButton()
        }
        else if (collectionName == "wallpaper" && photoName == "wall7") {
            photoView.setBackgroundResource(R.drawable.wall7)
            favoriteBtnClick()
            backButton()
        }
        else if (collectionName == "fashion" && photoName == "fashion1") {
            photoView.setBackgroundResource(R.drawable.fashion1)
            favoriteBtnClick()
            backButton()
        }
        else if (collectionName == "fashion" && photoName == "fashion2") {
            photoView.setBackgroundResource(R.drawable.fashion2)
            favoriteBtnClick()
            backButton()
        }
        else if (collectionName == "fashion" && photoName == "fashion6") {
            photoView.setBackgroundResource(R.drawable.fashion6)
            favoriteBtnClick()
            backButton()
        }
        else if (collectionName == "fashion" && photoName == "fashion7") {
            photoView.setBackgroundResource(R.drawable.fashion7)
            favoriteBtnClick()
            backButton()
        }
        else if (collectionName == "christmas" && photoName == "christmas1") {
            photoView.setBackgroundResource(R.drawable.christmas1)
            favoriteBtnClick()
            backButton()
        }
        else if (collectionName == "christmas" && photoName == "christmas2") {
            photoView.setBackgroundResource(R.drawable.christmas2)
            favoriteBtnClick()
            backButton()
        }
        else if (collectionName == "christmas" && photoName == "christmas6") {
            photoView.setBackgroundResource(R.drawable.christmas6)
            favoriteBtnClick()
            backButton()
        }
        else if (collectionName == "christmas" && photoName == "christmas7") {
            photoView.setBackgroundResource(R.drawable.christmas7)
            favoriteBtnClick()
            backButton()
        }
        else if (collectionName == "animation" && photoName == "animation1") {
            photoView.setBackgroundResource(R.drawable.animation1)
            favoriteBtnClick()
            backButton()
        }
        else if (collectionName == "animation" && photoName == "animation2") {
            photoView.setBackgroundResource(R.drawable.animation2)
            favoriteBtnClick()
            backButton()
        }
        else if (collectionName == "animation" && photoName == "animation6") {
            photoView.setBackgroundResource(R.drawable.animation6)
            favoriteBtnClick()
            backButton()
        }
        else if (collectionName == "animation" && photoName == "animation7") {
            photoView.setBackgroundResource(R.drawable.animation7)
            favoriteBtnClick()
            backButton()
        }




    }

    private fun favoriteBtnClick() {
        favoriteButton.setOnClickListener {
            favoriteButton.setBackgroundResource(R.drawable.favoritebuttonclicked)
            Toast.makeText(this, "Added to Favorite", Toast.LENGTH_SHORT).show()
        }

    }

    private fun backButton() {
        backButton.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
        }
    }

}


