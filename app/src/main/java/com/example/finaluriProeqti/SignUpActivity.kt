package com.example.finaluriProeqti

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_sign_up.*

class SignUpActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        supportActionBar?.hide()
        auth = Firebase.auth
        init()
    }
    private fun init(){
        signUpImageView.setOnClickListener {
            signUp()
        }
    }
    private fun signUp(){
        val email = regEmailEditText.text.toString()
        val password = regPasswordEditText.text.toString()
        val confirmPassword = confirmPasswordEditText.text.toString()
        if(email.isNotEmpty() && password.isNotEmpty() && confirmPassword.isNotEmpty()){
            if(password == confirmPassword){
                auth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this) { task ->
                        if (task.isSuccessful) {
                            // Sign in success, update UI with the signed-in user's information
                            d("TAG", "createUserWithEmail:success")
                            startActivity(Intent(this,LoginActivity::class.java))
                            finish()
                            val user = auth.currentUser
                        } else {
                            // If sign in fails, display a message to the user.
                            d("TAG", "createUserWithEmail:failure", task.exception)
                            Toast.makeText(baseContext, "Authentication failed.",
                                Toast.LENGTH_SHORT).show()
                        }

                        // ...
                    }

            }else{
                Toast.makeText(this,"Password is incorrect!",Toast.LENGTH_SHORT).show()
            }

        }else{
            Toast.makeText(this,"Please fill all fields!", Toast.LENGTH_SHORT).show()
        }
    }
}