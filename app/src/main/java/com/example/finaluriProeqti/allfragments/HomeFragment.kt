package com.example.finaluriProeqti.allfragments

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.finaluriProeqti.PhotoActivity
import com.example.finaluriProeqti.R
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment: Fragment(R.layout.fragment_home) {

    @SuppressLint("ResourceType")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (requireActivity() as AppCompatActivity).supportActionBar?.hide()


        init()

    }

    private fun init() {
        home1.setOnClickListener {
            photoView("home1")
        }
        home2.setOnClickListener {
            photoView("home2")
        }
        home3.setOnClickListener {
            photoView("home3")
        }
        home4.setOnClickListener {
            photoView("home4")
        }
        home5.setOnClickListener {
            photoView("home5")
        }
        home6.setOnClickListener {
            photoView("home6")
        }
        home7.setOnClickListener {
            photoView("home7")
        }
        home8.setOnClickListener {
            photoView("home8")
        }
        home9.setOnClickListener {
            photoView("home9")
        }
        home10.setOnClickListener {
            photoView("home10")
        }
    }
      fun photoView(string: String){
        val myActivity = (Intent(activity, PhotoActivity::class.java))
        myActivity.putExtra("value", string)
        activity?.startActivity(myActivity)
    }
    fun moveIntoFavorites(string: String){
        val myActivity = (Intent(activity, PhotoActivity::class.java))
        myActivity.putExtra("value", string)
        activity?.startActivity(myActivity)
    }
}