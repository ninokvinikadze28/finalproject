package com.example.finaluriProeqti.allfragments

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.finaluriProeqti.PhotoActivity
import com.example.finaluriProeqti.R
import com.example.finaluriProeqti.collectionActivity
import kotlinx.android.synthetic.main.fragment_search.*

class SearchFragments: Fragment(R.layout.fragment_search) {
    @SuppressLint("ResourceType")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (requireActivity() as AppCompatActivity).supportActionBar?.hide()

        init()

    }

    private fun init(){
        christmasCollectionImageView.setOnClickListener {
            collectionPhoto("christmas")
        }
        wallpaperCollectionImageView.setOnClickListener {
            collectionPhoto("wallpaper")
        }
        fashionCollectionImageView.setOnClickListener {
            collectionPhoto("fashion")
        }
        animationCollectionImageView.setOnClickListener {
            collectionPhoto("animation")
        }

    }

    fun collectionPhoto(string: String){
        val myActivity = (Intent(activity, collectionActivity::class.java))
        myActivity.putExtra("value", string)
        activity?.startActivity(myActivity)
    }

}