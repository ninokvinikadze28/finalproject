package com.example.finaluriProeqti.allfragments

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.example.finaluriProeqti.LoginActivity
import com.example.finaluriProeqti.Personinfo
import com.example.finaluriProeqti.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.fragment_profile.*

class ProfileFragment: Fragment(R.layout.fragment_profile) {

    private lateinit var mAuth: FirebaseAuth
    private lateinit var db: DatabaseReference

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (requireActivity() as AppCompatActivity).supportActionBar?.hide()

        mAuth = FirebaseAuth.getInstance()
        db = FirebaseDatabase.getInstance().getReference("UserInfo")


        exitButton.setOnClickListener {
            mAuth.signOut()
            startActivity(Intent(activity, LoginActivity::class.java))
        }

        saveButton.setOnClickListener {
            val name = nameEditText.text.toString()
            val url = profileUrlEditText.text.toString()

            val p = Personinfo(name, url)

            if (mAuth.currentUser?.uid != null) {
                db.child(mAuth.currentUser?.uid!!).setValue(p).addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        Toast.makeText(activity, "წარმატებით შეინახა", Toast.LENGTH_SHORT).show()
                        nameEditText.text = null
                        profileUrlEditText.text = null
                    } else {
                        Toast.makeText(activity, "დაფიქსირდა შეცდომა", Toast.LENGTH_SHORT).show()
                    }
                }

            }
        }

        if (mAuth.currentUser?.uid != null) {
            db.child(mAuth.currentUser?.uid!!).addValueEventListener(object: ValueEventListener {
                override fun onCancelled(error: DatabaseError) {
                    Toast.makeText(activity, "დაფიქსირდა შეცდომა", Toast.LENGTH_SHORT).show()
                }

                override fun onDataChange(snapshot: DataSnapshot) {
                    if (getActivity() == null) {
                        return
                    }

                    val p = snapshot.getValue(Personinfo::class.java)
                    if (p != null) {
                        // Name
                        nameTextView.text = p.name

                        // Profile Photo
                        Glide.with(this@ProfileFragment)
                            .load(p.imageUrl)
                            .centerCrop()
                            .placeholder(R.drawable.profilephoto)
                            .into(profileView)

                    }
                }

            })

        }



    }
}